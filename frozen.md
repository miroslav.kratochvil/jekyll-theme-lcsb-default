---
layout: frozen
title: frozen
order: 4
---

# This is an example of a Frozen Page layout

{% rtitle Modeling Parkinson's disease in midbrain-like organoids %}
Please cite the article on [NPJ Parkinson's disease](https://doi.org/10.1038/s41531-019-0078-4).
Lisa M Smits†, Lydia Reinhardt†, Peter Reinhardt, Michael Glatza, Anna S Monzel, Nacny Stanslowsky, Marcelo D Rosato-Siri, Alessandra Zanon, Paul M Antony, Jessica Bellmann, Sarah M Nicklas, Kathrin Hemmer, Xiaobing Qing, Emanuel Berger, Norman Kalmbach, Marc Ehrlich, Silvia Bolognin, Andrew A Hicks, Florian Wegner, Jared L. Sterneckert* & Jens C Schwamborn*
{% endrtitle %} 


{% rgridblock a-unique-id %}
{% rblock  Raw data | fas fa-file-download %}
The complete **Dataset** is available [here](https://webdav-r3lab.uni.lu/public/data/modeling-parkinsons-disease-in-midbrain-like-organoids/).
{% endrblock %}


{% rblock source code %}
The source code used to make the publication is available on [Github](https://github.com/LCSB-DVB/Smits_Reinhardt_2019) where you can traceback what have been done by the authors.
{% endrblock %}



{%  rblock microarray data | fas fa-globe %}
Data is accessible through [NCBI GEO](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE127967) website.
{% endrblock %}
{% endrgridblock %} 



