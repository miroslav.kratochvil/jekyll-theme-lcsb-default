---
layout: frozen
title: development
order: 5
---

# Note
This notes are intended for developers of the theme, and **not the developers of the website that use the theme**!

## Installation
Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "jekyll-theme-lcsb-default"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: jekyll-theme-lcsb-default
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-theme-lcsb-default


## Plugins

Plugins that are placed in `_plugins` directory **are not published automatically!**. In order to add a new one, add it to [https://git-r3lab.uni.lu/-/ide/project/core-services/jekyll-theme-lcsb-frozen-components](https://git-r3lab.uni.lu/-/ide/project/core-services/jekyll-theme-lcsb-frozen-components).

## Usage

### How to configure your website
Open the `_config.yml` file.
Update:

* _baseurl_ entry with the name of your repository,
* _title_ entry with your website's title,
* _email_ entry with your email,
* _new_menu_ to true, if you want to have menu items fully underlined (feel free to check how the site looks with the both options),
* _date_ entry to the year you want to appear in the footer,
* (optional) _banner_ to the name of the folder containing banner images (more later in this readme file)
* (optional) _logo_ to "small" or "big", depending on how wide your banner images are

Save, and commit the file. Every time you commit and push to the repository, the new page will be rebuilt and served.


### How to add new pages to the website?
Create a new `.md` file inside the project's directory.
For example, you can copy `index.md` file (and you can refer to its contents as to a valid page).

It should contain the following header:

```markdown
---
layout: default
title: index
order: 1
---
```

* Warning! In case of pages that should be available in the menu, `permalink` item (described below) **IS MANDATORY**.
* Leave layout as `default`.
* The title attribute (here - `index`) is displayed in the menu, thus rename it to your needs.
* Order attribute (here - `1`) decides about the order in the menu - lower numbers come first.
* In addition, you can add `permalink` attribute, which will cause the page to be accessible by the given permalink. For example : `permalink: /something/` refers to `https://your-webpage-address.com/something`.

Don't forget to save and commit the file.


### Having links to external sites in the menu

There is a special layout type, called `external`. For example, to have a link redirecting you to google.com website, you can create a post with the following contents:

```yaml
layout: external
redirect_to: https://www.google.com
title: Title in menu
order: 4
```


### Customise images in the header
Create a folder in `assets/banners` directory, with `banner.svg`, `logos.svg` and `motto.svg` files (for reference, consult `assets/banners/frozen` directory. Do not exceed image dimensions). Update `_config.yml` file with the name of directory you created for the images.
In case `logos.svg` file is wide, change `logo: small` to `logo: big` in `_config.yml`.

### Enabling and configuring pagination
The template includes `jekyll-paginate-v2` plugin by default, but it's turned off.
To use it, configure the pagination following instructions from the next paragraph, go to `pagination.md` file and change `enabled: false` to `enabled: true`, and `published: false` to `published: true`. Later, create a directory called `_posts` in the project root directory and fill it with posts.
You may also need to disable showing `index.md`, by setting `published: false` in its contents.

There are two sections in `_config.yml`, that refer to pagination: first, `  - jekyll-paginate-v2` line in plugins section, and then the configuration dictionary:

```yaml
pagination:
  enabled: true  # Note, that setting it to disabled does not disable it completely, as it has to be also set to false in `pagination.md` file
  title: ':title - page :num of :max'  # Customize the text
  per_page: 8  # How many posts should be displayed on one page
  permalink: '/page/:num/'  # The URL to the index of pagination
  limit: 0
  sort_field: 'date'  # How the posts should be sorted. can also be: `title` or any page attribute
  sort_reverse: true
  trail:  # How many pages should be shown in paginator.
    before: 2  # Show 2 before the current one, e.g. `< 5 6 CURRENT ...`
    after: 2  # Show 2 after the current one, e.g. `... CURRENT 6 7 >`
```

To disable it completely, set `enabled` to `false`, remove the aforementioned sections from the configuration, and `gem "jekyll-paginate-v2", "~> 1.7"` from `Gemfile` (from the project's root), and remove `pagination.md` file from project's root directory.
Refer to its [documentation](https://github.com/sverrirs/jekyll-paginate-v2/blob/master/README-GENERATOR.md) for more detailed instructions.

### Analytics

To enable the LCSB Anaytics Platform, add the following lines to your Jekyll site:

```yaml
banner_text: >-
  This website needs some cookies and similar means to function.<br>
  If you permit us, we will use those means to collect data on your visits for aggregated statistics to improve our service.
banner_accept_text: >-
  Accept cookies for aggregated statistics
banner_refuse_text: >-
  No thanks, only technically necessary cookies
banner_more_text: >-
  More information
cookies_expire: 180 # days
analyticsurl: https://analytics.lcsb.uni.lu/hub/
siteID: -1 # you get this siteID (>0) by sending an email to lcsb-r3@uni.lu
```

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, `_sass` and `assets` tracked with Git will be bundled.
To add a custom directory to your theme-gem, please edit the regexp in `jekyll-theme-lcsb-default.gemspec` accordingly.

## Automatic gem publishing
Gitlab CI is configured to automatically build and publish a new version of theme, whenever the commit is tagged.
For details refer to [the documentation of ruby gems](https://guides.rubygems.org/make-your-own-gem/), and this repository's `.gitlab-ci.yml` and settings.
