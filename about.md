---
layout: default
title: about
order: 3
show_print_button: true
---

# LCSB jekyll about page

<div class="index-box-container">
	<div class="index-box">
		<h3>Ruby packages</h3>
		<ul>
  			<pre>gem "jekyll-feed"</pre><br /><br /> 
			<pre>gem "jekyll-paginate-v2"</pre><br /><br /> 
			<pre>gem "jekyll-theme-lcsb-frozen-components"</pre><br /><br /> 
  		</ul>
	</div>
	<div class="index-box">
		<h3>Bundler</h3>
		<ul>
			Bundler 2.1.2
		</ul>
	</div>
	<div class="index-box">
		<h3>C</h3>
		<ul>
			Way longer text
		</ul>
	</div>
	<div class="index-box">
		<h3>D</h3>
		<ul>
			Howto cards
		</ul>
	</div>
	<div class="index-box">
		<h3>E</h3>
		<ul>
			Lorem ipsum
		</ul>
	</div>
</div>
